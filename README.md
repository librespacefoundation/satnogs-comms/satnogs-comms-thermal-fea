# SatNOGS COMMS Termal FEA

Thermal simulation using finite element analysis method for the SatNOGS COMMS board
  
![alt text](output_render.png "Rendered output")
  
![alt text]( output_render_filtered.png "Rendered output filtered")
